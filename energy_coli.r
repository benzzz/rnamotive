# Need to pre load Vienna and pandoc(for markdown file creation)
# 'module add ViennaRNA/2.3.1'

library(ape)
library(stringi)
library(stringr)
library(Biostrings)
library(tidyverse)
library(seqinr)
library(reshape2)

source("help_finctions.r")


## inputs
genomefilename = "GCF_000005845.2_ASM584v2_genomic.fna"
gfffilename = "GCF_000005845.2_ASM584v2_genomic.gff"
vinInputs = "ecoliVienna" 
intrestgenes = "e_coli.csv"


## 1 read-data
# Load FASTA file.
fasta = read.FASTA(genomefilename)
names(fasta) = stri_extract_first_regex(names(fasta), pattern = "[^ ]*", perl = T)
fasta = lapply(fasta, as.character.DNAbin)
# Silly way to look on relevant genes only from previous list.
trRNA = read.csv(intrestgenes)
trRNA$ID = gsub(trRNA$X, pattern = "e_coli_", replacement = "")
# Load GFF file.
bac_gff = read.gff(gfffilename)
bac_gff$ID = str_match(bac_gff$attributes, "ID=[a-z0-9_]*")
bac_gff$ID = gsub(bac_gff$ID, pattern = "ID=", replacement = "")
bac_gff$ID = factor(bac_gff$ID)


##  get genes sequences
  
## initialize fasta to empty values
  bac_gff$fasta = ""
## b. Attach genes with matching sequence
  for (index in seq_along(bac_gff$ID)) {
        bac_gff$fasta[index] = getSequence(fasta, index, bac_gff)
          bac_gff$geneLength[index] = nchar(bac_gff$fasta[index])
  }

      genes = left_join(trRNA, bac_gff, by = "ID")
      genes = genes[!is.na(genes$fasta),]
      genes = genes[!is.na(genes$fasta),]
      trRNA = genes[genes$count > 2,]
# remove big unnecessary objects
# rm(fasta, bac_gff, nona_trRNA)

## create-file-sequences
dir.create(vinInputs)
for(index in seq(genes$ID)){
write.fasta(genes$fasta[index]
  , genes$attributes[index]
  , file.out = paste0(vinInputs,"/", genes$ID[index], ".fa")
  , open = "w"
  , nbchar = 60)
}


## send-vienna Very long (about 12 hours or more per bacteria)
sequences =  list.files(vinInputs, full.names=T, pattern = "*.fa")
# optional to remove big unreal sequences
#
sequences = sequences[file.size(sequences) < 150]
output = paste0(str_match(sequences, pattern = "[^.]*"), ".out")
params = "-p -d2 --noLP --noPS -i"
for(index in seq(sequences)){
  cmd = paste("RNAfold", params, sequences[index], ">", output[index])
  system(cmd)
}


## parse-results
outputs = list.files(vinInputs, full.names = T, pattern = "*out")
outputs = outputs[file.size(outputs) > 0]
IDs = gsub(pattern = ".out", x = basename(outputs), replacement = "")
results = tibble(ID = IDs)

pat = "-[\\d]*\\.[\\d]*"
for(index in (seq(outputs))){
  conn = file(outputs[index])
  lines = readLines(conn)
# return the first number from the vienna output, should be minimum free energy
  results$mfe[index] = unlist(str_match_all(lines, pattern = pat))[1]
  close(conn)
}
results$mfe = as.numeric(results$mfe)


## conclude}
write.csv(results, row.names = F, file = paste0(vinInputs, ".csv"))
#file.remove(outputs)
file.remove(sequences)








# ## Tsviya code
# #perl reference, include = F}
# #!
# # runs RNAfold from the Vienna package. Not parallel.
# # have to ==> module add ViennaRNA/2.3.1
# use Bio::SeqIO;
# 
# my($infile) = @ARGV;
#  
# my $inseq = Bio::SeqIO->new(-file   => "$infile",
#                            -format => 'fasta' );
#
# 
# while (my $seq = $inseq->next_seq) {
#   $header=$seq->id();
#   @data = split(/\|/,$header);
#  ($name,$version)=$header=~/hg38_ncbiRefSeq_([A-Z\_\d]+)\.(\d+)/;
#  print "$name,$version\n";
#  $ViennaInput = "out_5UTR_b/$name".".fa";
#  $ViennaOutput = "out_5UTR_b/$name".".out";
#  $psFile = "hg38_ncbiRefSeq_"."$name"."."."$version"."_dp.ps";
#  my $outseq = Bio::SeqIO->new(-file   => ">$ViennaInput",
#                            -format => 'fasta' );
#  $outseq->write_seq($seq);
# 
#  # run RNAfold
#  system("RNAfold -p -d2 --noLP --noPS -i $ViennaInput > $ViennaOutput");
#  system("rm $psFile");
# }
# 
# 
# How to parse:
# #!
#  
# #my($infile) = 'out_5UTR_ub/HSPE1.out';
#  
# (@files) = `ls -1 out_5UTR_g/*.out`;
# chomp(@files);
#  
#
# foreach $infile (@files){
#   open(IN,"$infile")|| warn "no input file=> $infile\n";
#   $line = <IN>;
#   GO:while($line=<IN>){
#     if($line =~/\d+/){
#  
#     chomp($line);
#       ($min_FreeEnergy) = $line =~/(\-\d+\.\d+)/;
#       print "$infile\t$min_FreeEnergy\n";
#       last GO;
#   
#     }
#   }
#   close(IN);
# }
