library(stringr)
library(stringi)
library(Biostrings)

subseq = function(nucleotides, start, end){
  ## return sub sequence of charcter array
  # sequence = start:end
  #fullsequence = as.character.DNAbin(nucleotides)[[1]]
  fullsequence = nucleotides[[1]]
  bool = rep(FALSE, length(fullsequence))
  bool[start:end] = TRUE
  subseq = fullsequence[bool]
  paste(subseq, collapse = "")
}

getSequence = function(fna, id, gff){
  ## get sequence of dna of gene from id in gff file and fasta file (as array)
  fasta = fna[gff$seqid[id]]
  sequence = subseq(fasta, gff$start[id], gff$end[id])
  if(gff$type[id] != "CDS") {
    sequence
  }
  if(gff$strand[id] == "+") {
    as.character(DNAString(sequence))
  }
  else {
    as.character(reverseComplement(DNAString(sequence)))
  }
}

longest.true = function(x) max(rle(x)$lengths * rle(x)$values)

longest.motive = function(sequence, motive){
  ## return the longest sequel of specific motive of 3 repetitions
  spliceSequence = unlist(str_extract_all(sequence, "[TCGA]{3}"))
  bool = grepl(x = spliceSequence, pattern = motive)
  return(longest.true(bool))
}
